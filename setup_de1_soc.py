#!/usr/bin/python3
from subprocess import Popen, PIPE
import os
import sys
from os import path

QUARTUS_DIR = "/home/thomas/intelFPGA/20.1/quartus/"
PRELOADER_FILE = "/home/thomas/intelFPGA/20.1/University_Program/Monitor_Program/arm_tools/u-boot-spl.de1-soc.srec"

def run(command):
    
    my_env = os.environ.copy()
    my_env["PATH"] = QUARTUS_DIR + "/bin64/:" + my_env["PATH"]

    process = Popen(command, env=my_env, stdout=PIPE, shell=True)

    while True:
        line = process.stdout.readline().rstrip()
        if not line:
            break
        
        # Kill when waiting for gdb connection
        if line == b'Starting GDB Server.':
            process.kill()
            break

        yield line

def printHelp():
    print('\033[0m', "Usage: python3", sys.argv[0], "[sof file]")
    print('\033[0m', "Load a sof file and preloader in the DE1_SoC\n")
    print('\033[0m', "sof file is required")
    print('\033[0m', "-h for help")

if __name__ == "__main__": 

    if len(sys.argv) < 2:
        print('\033[91m', "ERROR: ", "sof file is missing")
        printHelp()
        exit()
    
    if sys.argv[1] == '--help' or sys.argv[1] == '-h':
        printHelp()
        exit()
    elif sys.argv[1][0] == '-':
        print('\033[91m', "ERROR: ", "unknown option")
        printHelp()
        exit()

    sof_file = sys.argv[1]

    if not path.exists(sof_file):
        print('\033[91m', "ERROR: ", sof_file, "does not exist..")
        exit()

    for output in run("quartus_pgm -c 1 -m jtag -o \"P;" + sof_file + "@2\""):
        print('\033[92m', output.decode())

    for output in run("quartus_hps -c 1 -o GDBSERVER --gdbport0=2843 --preloader="+PRELOADER_FILE+" --preloaderaddr=0xffff13a0"):
        print('\033[92m', output.decode())

