# Installation - Manuel ARM DS 5

**Téléchargement:** https://developer.arm.com/tools-and-software/embedded/arm-development-studio/downloads

Prendre la version **Development Studio 2020.1-1 for Linux 64-bit**

## Liens utiles

- https://rocketboards.org/foswiki/Documentation/SoCEDS

## Installation

Extraire l'archive **DS000-BN-00001-r20p1-00rel2.tgz**

**Exécuter:**

```sh
sudo ./armds-2020.1-1.sh
```

Après l'installation, il est possible qu'il manque certaine librairies, cela n'est pas un problème dans notre cas d'utilisation.

### Licence

Elle doit être reconnue automatiquement si la machine est connectée au réseau de l'école en allant dans **Help -> Arm Liscence Manager...**

Sinon elle peut être ajoutée manuellement au même endroit en cliquant sur **Add** (sélectionner **Add product license**) puis **Next** et dans **License Server** ajouter :

```sh
8224@eilic01
```

### Démarrer ARM DS-5

**Remarque:** il est important d'avoir la variable d'environnement **QUARTUS_ROOTDIR=/xxx/intelFPGA/20.1/quartus** permettant, plus tard, la connexion JTAG à la DE1-SoC.

Pour démarrer le programme il suffit de cliquer sur l'icône créé sur le bureau ou bien de lancer l'exécutable **xxx/arm/developmentstudio-2020.1-1/bin/armds_ide**

### Ajout de la toolchain

**Remarque:** Nous utilisons la toolchain fournit par le programme **Altera Monitor Program** (Version 18.x trouvé sur cyberlearn **21_HEIG-VD_SOCF**) se trouvant dans **xxx/intelFPGA/20.1/University_Program/Monitor_Program/arm_tools/baremetal/bin** il est donc nécessaire de l'avoir installé.

Dans l'onglet Window -> Preferences -> Arm DS -> Toolchains

<img src="./img/image-20220516162237751.png" alt="image-20220516162237751" style="zoom:80%;" />

Cliquez sur **Add...** puis **Browse...** et allez chercher le dossier **xxx/intelFPGA/20.1/University_Program/Monitor_Program/arm_tools/baremetal/bin** contenant tout les exécutables *arm-altera-eabi-\**. Cliquez sur **Next >**, la Toolchain devrait être reconnue, laissez tout pour défaut et cliquez sur **Finish**.



## Création d'un projet

1. Dans l'onglet File -> New -> Project... choisissez C/C++ -> C Project puis **Next >**.

2. Choisissez le nom du projet, la location et la bonne toolchain de la manière suivante

   <img src="./img/image-20220516165629719.png" alt="image-20220516165629719" style="zoom:80%;" />

3. Vous pouvez ensuite tout laisser par défaut et cliquer sur **Next >** puis **Next >** et enfin **Finish**
4. Vous pouvez ajouter un fichier source en faisant clique droite sur le projet dans "**Project Explorer**" -> New -> File. Ou ajouter un fichier C existant en le mettant simplement dans le dossier du projet.
5. Pour Build le projet sans erreur, il est nécessaire d'ajouter le script de linkage lié au **Cyclone V**. Rendez vous dans le propriétés du projet en faisant clique droite sur le projet -> properties

<img src="./img/image-20220517084617828.png" alt="image-20220517084617828" style="zoom:80%;" />

Dans C/C++ Build -> Settings -> GCC C Linker -> Image, cliquez sur Browse et allez chercher ce fichier se trouvant dans le dossier d'installation de **Altera Monitor Program** :`xxx/intelFPGA/20.1/University_Program/Monitor_Program/arm_tools/baremetal/arm-altera-eabi/lib/cycloneV-dk-ram-hosted.ld`

6. Vous pouvez maintenant Build correctement votre projet en cliquant sur le petit marteau en haut à gauche de la partie "**Project Explorer**".

## Création d'une configuration de Debug

Afin de pouvoir lancer et debugger votre programme il est nécessaire de créer une configuration propre à votre projet.

Allez dans l'onglet **Run** puis **Debug configurations...**

Afin de pouvoir se connecter en JTAG à la DE1_SoC, il est nécessaire d'avoir préalablement effectué ces étapes :

   1. Ajouter les règles udev :

      Créer le fichier `/etc/udev/rules.d/51-altera-usb-blaster.rules` avec le contenu suivant

      ```sh
      SUBSYSTEM=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6001", MODE="0666"
      SUBSYSTEM=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6002", MODE="0666"
      SUBSYSTEM=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6003", MODE="0666"
      SUBSYSTEM=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6010", MODE="0666"
      SUBSYSTEM=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6810", MODE="0666"
      ```

      utilisez la commande `sudo udevadm control --reload` pour reload votre configuration.

   2. la variable d'environnement `QUARTUS_ROOTDIR`n'existe pas

      ```sh
      # 1. l'ajouter dans l'environnement actuel
      export QUARTUS_ROOTDIR=/xxx/intelFPGA/20.1/quartus
      
      # 2. l'ajouter de façon permanente et global: ajoutez dans le fichier /etc/environment la ligne
      QUARTUS_ROOTDIR=/xxx/intelFPGA/20.1/quartus
      #nécessite un log out/in
      ```

### Onglet `Connection` 

<img src="./img/image-20220517103602665.png" alt="image-20220517103602665" style="zoom:80%;" />

1. Créez une nouvelle configuration de type **Generic ARM C/C++ Application**

2. Donnez un nom à votre configuration

3. Choisissez la plateforme: Intel SoC FPGA -> Cyclone V SoC (Dual Core) -> Bare Metal Debug -> Debug Cortex-A9_0

4. Sélectionnez le bon type de connection par JTAG

5. Cliquez sur **Browse...** et après quelque secondes la connexion devrait apparaître. (La carte doit être branchée et allumée)

### Onglet `Files`

Il est important d'avoir build votre program avant cette étape.

<img src="./img/image-20220517105604017.png" alt="image-20220517105604017" style="zoom:80%;" />

Il suffit simplement de sélectionner le ficher `.axf` se trouvant dans le dossier **Debug** générer lors du Build du projet et de cocher la case **Load Symbols**

### Onglet `Debugger`

<img src="./img/image-20220517105939582.png" alt="image-20220517105939582" style="zoom:80%;" />

Cochez simplement **Debug from symbol** et mettant votre fonction d'entrée (**main** en général). Cela ajoutera un breackpoint à l'entrée de la fonction **main**.

## Lancer / Debuger le programme C

**IMPORTANT**: avant de pouvoir vous connecter à la carte, il est nécessaire de lancer le petit programme python fourni afin de charger le fichier `.sof` générer par Quartus ainsi que le preloader du HPS. Script python : https://gitlab.com/trr-reds/py_utils_socf/-/blob/main/setup_de1_soc.py

Afin de pouvoir utiliser la commande **printf**, il est nécessaire d'ajouter les lignes suivantes avec la fonction main:

```c
#include <stdio.h>

int __auto_semihosting;
```



![image-20220517111512238](./img/image-20220517111512238.png)

1. le panel **Debug Control** contient toutes les actions pouvant être effectuées durant votre debug.
2. Il vous faut sélectionner la configuration de debug que vous avez du créer auparavant.
3. Le petit bouton correspondant à une prise vous permet de vous connecter et de lancer le debug de votre programme.

En cliquant sur le bouton de l'encadré `3` vous arriverez sur la fenêtre suivante:

![image-20220517112506060](./img/image-20220517112506060.png)

Vous pouvez ensuite depuis le panel **Debug Control** effectuer tout type d'opération :

- Continuer l'exécution du programme <img src="./img/image-20220517113127087.png" alt="image-20220517113127087" style="zoom:80%;" /> cela poursuit l'exécution du programme jusqu'au prochain breakpoint
- Remettre le programme en pause <img src="./img/image-20220517113250093.png" alt="image-20220517113250093" style="zoom:80%;" /> pour lire la mémoire (onglet **Memory**), il est nécessaire d'avoir mis le programme en pause
- Exécuter le programme ligne par ligne <img src="./img/image-20220517113114932.png" alt="image-20220517113114932" style="zoom:80%;" />
- Retourner au début du main <img src="./img/image-20220517113024141.png" alt="image-20220517113024141" style="zoom:80%;" />
- Vous déconnecter de la carte <img src="./img/image-20220517113355149.png" alt="image-20220517113355149" style="zoom:70%;" />



## Projet avec interruptions

### Modifier le linker script

![](img/ld_script_with_vector.png)

Modifier le script par **xxx/intelFPGA/20.1/University_Program/Monitor_Program/build/altera-socfpga-hosted-with-vectors.ld** ce fichier attend plusieurs flags de linkage

### Ajout des flags nécessaire demandé par le linker

![](img/add_linker_misc.png)

Dans **other flags**, ajouter les flags suivant :

```sh
-Wl,--defsym  -Wl,arm_program_mem=0x40  -Wl,--defsym  -Wl,arm_available_mem_size=0x3fffffb8  -Wl,--defsym  -Wl,__cs3_stack=0x3ffffff8  -Wl,--section-start  -Wl,.vectors=0x0
```

Cela permet de passer des options au script de linkage

